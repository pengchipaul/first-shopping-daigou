<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;



class CheckAdmin
{
    /**
     * Handle an incoming request.
     * redirect to home if user is not admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::find(Auth::id())['admin'] != 1){
            return redirect('home');
        }
        return $next($request);
    }
}
