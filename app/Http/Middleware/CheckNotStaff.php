<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;



class CheckNotStaff
{
    /**
     * Handle an incoming request.
     * redirect to staff index page if user is staff
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::find(Auth::id())['staff'] != 0){
            return redirect(route('staff_show'));
        }
        return $next($request);
    }
}
