<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;

use App\Http\Controllers\ProductController;

class CategoryController extends Controller
{
    /**
     * show one category and its products
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category_show(Request $request)
    {
        $categories = Category::all();
        $category = Category::find($request->id);

        /*
         * get category products and their tags
         */
        foreach($category->products as $product){
            $product['tag_img_url'] = ProductController::get_tag_img($product);
        }
        $category['products'] = $category->products;
        $page = 'products';
        return view('home.categories.show', compact('category', 'page','categories'));
    }
}
