<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Brand;

use App\Http\Controllers\ProductController;

class BrandController extends Controller
{
    /** display one brand and products of that brand
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function brand_show(Request $request)
    {
        $brand = Brand::find($request->id);

        /*
         * get products and their tags(hot, new and etc.)
         */
        foreach($brand->products as $product){
            $product['tag_img_url'] = ProductController::get_tag_img($product);
        }
        $brand['products'] = $brand->products;
        $page = 'products';
        return view('home.brands.show', compact('brand', 'page'));
    }

    /** get other brands that are not the one of the 9 default brands
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_other_brands()
    {
        $brands = Brand::all()->where('order', '==', null)->sortBy('id');
        return response()->json($brands, 200);
    }
}
