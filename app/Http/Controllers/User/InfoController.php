<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfoController extends Controller
{
    /**
     * render question page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function questions(){
        $page = "dashboard";
        return view('home.info.questions',compact('page'));
    }

    /**
     * render about us page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about_us(){
        $page = "dashboard";
        return view('home.info.about_us',compact('page'));
    }

    /**
     * render contact us page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact_us(){
        $page = "dashboard";
        return view('home.info.contact_us',compact('page'));
    }

    /**
     * render coming soon page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function coming_soon(){
        return view('home.info.coming_soon');
    }
}
