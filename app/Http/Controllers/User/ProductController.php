<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Product;
use App\Brand;
use App\Category;
use App\Favourite;

class ProductController extends Controller
{
    /**
     * render single product page, includes brand, category and favourite/likes
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function product_show(Request $request)
    {
        $user = Auth::user();
        $page = "products";
        $product = Product::find($request->id);
        $product['brand'] = Brand::find($product->brand_id);
        $product['category'] = Category::find($product->category_id);
        if($user == null){
            $product['like'] = 0;
        } else {
            $like = Favourite::where('user_id', $user->id)->where('product_id', $request->id)->first();
            if ($like) {
                $product['like'] = 1;
            } else {
                $product['like'] = 0;
            }
        }

        return view('home.products.show', compact('product', 'page'));
    }

    /**
     * get single product data, includes brand, category and favourites/likes
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_product(Request $request)
    {
        $user = Auth::user();
        $product = Product::find($request->id);
        $product['brand'] = Brand::find($product->brand_id);
        $product['category'] = Category::find($product->category_id);
        $like = Favourite::where('user_id', $user->id)->where('product_id', $request->id)->first();
        if ($like) {
            $product['like'] = 1;
        } else {
            $product['like'] = 0;
        }
        return response()->json($product, 200);
    }

    /**
     * update favourites
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_like(Request $request)
    {
        $user = Auth::user();
        $favourite = Favourite::where('user_id', $user->id)->where('product_id', $request->product_id)->first();
        if ($favourite) {
            $favourite->delete();
            return response()->json('已取消收藏', 200);
        } else {
            Favourite::create(['user_id' => $user->id,
                'product_id' => $request->product_id]);
            return response()->json('收藏成功', 200);
        }
    }

    /**
     * display single product detail page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function product_detail(Request $request)
    {
        $page = 'products';
        $product = Product::find($request->id);
        return view('home.products.detail', compact('page', 'product'));
    }

    /**
     * search product
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search_products(Request $request)
    {
        $page = 'products';
        $search_query = $request->search_query;
        if ($search_query == 'price_up') {
            $products = Product::orderBy('price')->get();
        } elseif ($search_query == 'price_down') {
            $products = Product::orderBy('price', 'desc')->get();
        } elseif ($search_query == 'sale_down') {
            $products = Product::orderBy('sale', 'desc')->get();
        } else {
            $products = Product::search($search_query)->get();
        }
        return view('home.searches.show', compact('products', 'page', 'search_query'));
    }

    /**
     * render favourite page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function favourite_index()
    {
        $page = 'dashboard';
        return view('home.favourites.index', compact('page'));
    }

    /**
     * get favourites data
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_fav()
    {
        $user = Auth::user();
        $fav_products = $user->fav_products;
        return response()->json($fav_products, 200);
    }
}
