<?php

namespace App\Http\Controllers\Custom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;

class AdminController extends Controller
{
    /**
     * @param $brand_array, array of brand ids
     * @param $date
     */
    public static function change_date($brand_array, $date){
        /*
         * change expired date of brands
         */
        foreach ($brand_array as $brand_id){
            $brand = Brand::find($brand_id);
            foreach ($brand->products as $product){
                $product->expired_date = $date;
                $product->save();
            }
        }
    }
}
