<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Order;
use App\Service;
use App\Brand;
use App\Category;

class StaffController extends Controller
{
    /**
     * show admin or staff login interface
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admin_login(){
        return view('admin.login');
    }

    /**
     * show data to staff according to their job
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(){
        $user = Auth::user();

        /*
         * get staff job
         */
        if($user->staff == 1){
            /*
             * get uncompleted orders for retail staff
             */
            $orders = Order::all()->where('status','1');
            foreach($orders as $order){
                $order['sending_address'] = $order->address;
                $order['sender_address'] = $order->address;
                $order['products'] = $order->products;
                foreach($order['products'] as $product){
                    $product['num'] = $product->get_order_quantity($order->id);
                }
            }

            /*
             * this is for beta staff interface, it only shows when it's retail staff and the login is 'test'
             */
            if($user->email == 'test'){
                $testing = true;
            } else {
                $testing = false;
            }
            return view('staffs.retail.show',compact('orders','testing'));
        } elseif ($user->staff == 2) {

            /*
             * show customer service requests to financial staff
             */
            $services = Service::all()->where('status','0');
            return view('staffs.finance.show',compact('services'));
        } elseif ($user->staff == 3){

            /*
             * show product information to product management staff
             */
            $products = Product::all();
            return view('staffs.product.show',compact('products'));
        }
    }

    /** retail staff uploads delivery info
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function upload_delivery_no(Request $request){
        $user = Auth::user();

        /*
         * make sure the order is an uncompleted order, status 1 for 'uncompleted'
         */
        try{
            $order = Order::find($request->order_id)->where('status','1')->first();
        } catch (\Exception $e){
            Log::debug($e);
            return redirect(route('staff_show'))->with('fail','订单未找到');
        }

        /*
         * save delivery numbers as an array, redirect to staff home page if format is incorrect
         */
        try{
            $order->delivery_no = implode("|",explode(';',$request->delivery_no));
        } catch (\Exception $e){
            return redirect(route('staff_show'))->with('fail','订单号格式错误');
        }

        /*
         * upload addon images
         */
        if ($request->has('images') && $files = $request->file('images')) {
            $images = array();
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $img_path = $file->store('orders/addon');
                    $images[] = $img_path;
                } else {
                    return redirect(route('staff_show'))->with('fail', '增值服务图片上传失败');
                }
            }
            $order->images = implode("|", $images);
        }

        /*
         * upload delivery images
         */
        try{
            $files = $request->file('deli_images');
            $images = array();
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $img_path = $file->store('orders/delivery_images');
                    $images[] = $img_path;
                } else {
                    return redirect(route('staff_show'))->with('fail', '物流单号图片上传失败');
                }
            }
            $order->deli_images = implode("|", $images);

        } catch (\Exception $e){
            return redirect(route('staff_show'))->with('fail', '物流单号图片上传失败');
        }

        /*
         * if files are valid, update order info
         */
        $order->delivery_fee = $request->get('delivery_fee');
        $order->status = 2;
        $order->delivery_staff_id = $user->id;
        $order->save();
        $order->weight > 1000 ? $delivery_fee = $order->weight * config('delivery.rate') : $delivery_fee = 1000 * config('delivery.rate');
        if($order->delivery_fee < $delivery_fee){
            $customer = $order->user;
            $customer->balance += ($delivery_fee - $order->delivery_fee);
            $customer->save();
        }
        return redirect(route('staff_show'))->with('success','物流信息上传成功');
    }

    /** financial staff uploads customer service request feedback
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function upload_service(Request $request){
        $service = Service::find($request->service_id);
        $service->result = $request->result;
        $service->status = 1;
        $service->save();
        return redirect(route('staff_show'))->with('success','处理售后申请成功');

    }

    /** product management staff creates products page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create_product(){
        $brands = Brand::all();
        $categories = Category::all();
        return view('staffs.product.create',compact('brands','categories'));
    }

    /** product management staff uploads product
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store_product(Request $request)
    {
        $product = new Product();

        /*
         * validates image file
         */
        if($request->hasfile('img_url'))
        {
            if($request->file('img_url')->isValid()){
                $img_path = $request->file('img_url')->store('product_img');
            } else {
                return redirect('products/create')->with('fail','图片上传失败');
            }

        }
        $product->name = $request->get('name');
        if(isset($img_path)){
            $product->img_url = $img_path;
        }

        /*
         * upload other info
         */
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->weight = $request->get('weight');
        $product->category_id = $request->get('category_id');
        $product->brand_id = $request->get('brand_id');
        $product->pospal_id = $request->get('pospal_id');
        $product->save();

        return redirect(route('staff_show'))->with('success','已添加产品');
    }

    /** product management staff creates brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create_brand(){
        return view('staffs.product.brands.create');
    }

    /** product management staff uploads brand
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store_brand(Request $request){
        $brand = new Brand();

        /*
         * validates image file
         */
        if($request->hasfile('img_url'))
        {
            if($request->file('img_url')->isValid()){
                $img_path = $request->file('img_url')->store('brand_img');
            } else {
                return redirect('brands/create')->with('fail','图片上传失败');
            }

        }

        /*
         * store other info
         */
        $brand->name = $request->get('name');
        if(isset($img_path)){
            $brand->img_url = $img_path;
        }
        $brand->save();

        return redirect(route('staff_show'))->with('success','已添加品牌');
    }

    /** product management staff creates category page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create_category(){
        return view('staffs.product.categories.create');
    }

    /** product management staff uploads category
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store_category(Request $request){
        $category = new Category();

        /*
         * validates file image
         */
        if($request->hasfile('img_url'))
        {
            if($request->file('img_url')->isValid()){
                $img_path = $request->file('img_url')->store('category_img');
            } else {
                return redirect('categories/create')->with('fail','图片上传失败');
            }

        }

        /*
         * store other info
         */
        $category->name = $request->get('name');
        if(isset($img_path)){
            $category->img_url = $img_path;
        }
        $category->save();

        return redirect(route('staff_show'))->with('success','已添加类别');
    }
}
