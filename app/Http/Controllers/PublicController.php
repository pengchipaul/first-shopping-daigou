<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    /**
     * render public about us page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about_us(){
        return view('public.about_us');
    }

    /**
     * render public contact us page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact_us(){
        return view('public.contact_us');
    }
}
